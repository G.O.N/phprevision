<?php

require "begin.html";
?>
<h1> Removing a nobel prize </h1>
<?php
if (isset($_GET['id']) and !empty($_GET['id'])) {
    require_once "Model.php";
    $e = new Model;
    $ex = $e->remove_nobel_prize($_GET['id']);
    if (!$ex) {
        echo "The nobel prize has been removed.";
    } else {
        echo "There is no nobel prize with such id.";
    }
} else {
    echo "there is no id in the URL ";
}
